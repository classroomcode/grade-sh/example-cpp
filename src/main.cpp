#include <fstream>
#include <iostream>

#include "convert_to_binary.h"

int main(const int argc, const char *argv[]) {
  int num_to_convert;
  if (argc == 3) {
    std::ifstream infile(argv[1]);
    infile >> num_to_convert;
    infile.close();
    std::ofstream outfile(argv[2]);
    outfile << convert_to_binary(num_to_convert) << std::endl;
    outfile.close();
  } else {
    std::cin >> num_to_convert;
    std::cout << convert_to_binary(num_to_convert) << std::endl;
  }
}
