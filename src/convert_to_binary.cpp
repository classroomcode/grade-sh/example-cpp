#include "convert_to_binary.h"

std::string convert_to_binary(int num_to_convert) {
  // Purpose:    Converts a decimal number to a binary representation
  // Parameters: A decimal number, as an integer
  // User Input: No
  // Prints:     Nothing
  // Returns:    Result as a std::string of 1s and 0s
  // Modifies:   Nothing
  // Calls:      Basic python only
  // Tests:      ./unit_tests/*
  // Status:     Done!
  if (num_to_convert == 0) {
    return "0";
  }
  std::string result = "";

  while (num_to_convert > 0) {
    if (num_to_convert % 2 == 0) {
      result = "0" + result;
    } else {
      result = "1" + result;
    }
    num_to_convert /= 2;
  }
  return result;
}
