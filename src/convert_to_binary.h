#ifndef CONVERT_TO_BINARY_H
#define CONVERT_TO_BINARY_H
#include <string>

std::string convert_to_binary(int num_to_convert);

#endif