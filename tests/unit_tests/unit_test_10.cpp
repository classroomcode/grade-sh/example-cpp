#include "convert_to_binary.h"
#include "test_utils.hpp"

int main(const int argc, const char **argv) {
  return test_wrapper(argc, argv,
                      []() { return convert_to_binary(10) == "1010"; });
}